import { Injectable } from "@angular/core";
import {HttpClient} from '@angular/common/http';
import { Observable } from "rxjs";
import { ApiResponsePokemon, Pokemon } from "../shared/models/pokemon.model";

@Injectable({
    providedIn: 'root'
})

export class FetchPokemons{
    constructor(private readonly http: HttpClient){
    }

    public getPokemons$(url:string): Observable<ApiResponsePokemon> {
        return this.http.get<ApiResponsePokemon>(url);
    }

    public getPokemonsIndividually$(url: string): Observable<Pokemon> {
        return this.http.get<Pokemon>(url);
    }

    
}