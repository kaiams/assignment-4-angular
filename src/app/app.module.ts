import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AppHeader } from './components/header/app-header.component';
import { LoginPage } from './routes/login/app-login.page';
import { PokemonDeckPage } from './routes/pokemon-deck/pokemon-deck.page';
import { TrainerPage } from './routes/trainer/trainer.page';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPage,
    PokemonDeckPage,
    AppHeader
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
