import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginPage } from "./routes/login/app-login.page";
import { PokemonDeckPage } from "./routes/pokemon-deck/pokemon-deck.page";
import { TrainerPage } from "./routes/trainer/trainer.page";

const routes: Routes = [
    {
        path: '',
        component: LoginPage

    },
    {
        path: 'trainer',
        component: TrainerPage
    },
    {
        path: 'pokemon-deck',
        component: PokemonDeckPage
    }

]
@NgModule({
    imports: [ RouterModule.forRoot(routes)],
    exports: [RouterModule]
    
})

export class AppRoutingModule {}