import { Component } from "@angular/core";
import { Router } from "@angular/router";


@Component({
    selector: 'app-header',
    templateUrl: 'app-header.component.html',
    styleUrls: ['app-header.component.css']
})

export class AppHeader {
    readonly user = JSON.parse(localStorage.getItem("currentUser")!);

    constructor(private readonly router: Router){
    }

    public changePage(page: string){
        this.router.navigate([page]);
    }

    public logOut(){
        const user = JSON.parse(localStorage.getItem("currentUser")!);
        let allUsers = JSON.parse(localStorage.getItem("users")!);

        allUsers = allUsers.filter((existingUser:any) => existingUser.name !== user.name)
        allUsers.push(user);
        
        localStorage.setItem("users", JSON.stringify(allUsers));
        localStorage.removeItem("currentUser")
        this.router.navigate([""]);
    }

}