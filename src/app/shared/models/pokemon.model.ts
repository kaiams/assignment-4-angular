export interface ApiResponsePokemon{
    next: string,
    results: Pokemon[] 
}

export interface Pokemon{
    name: string,
    url: string,
    imgUrl: string, 
    catched: boolean,
    id: string,
    abilities: abilityInfo[]
}

interface abilityInfo{
    ability: ability
}

interface ability{
    name: string
}