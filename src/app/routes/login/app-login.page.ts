import { stringify } from "@angular/compiler/src/util";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
    selector: 'app-login',
    templateUrl: './app-login.page.html',
    styleUrls: ['./app-login.page.css']
})


export class LoginPage {
    ngOnInit(){
        //if user is logged in, redirect
        const user = localStorage.getItem("currentUser");
        if(user){
            this.loggedIn();
        }
    }

    constructor(private readonly router: Router) {
    }
  
    public loginUser(userName: string) {
        //if users does not exist in local storage allUsers is set to an empty array.
        const allUsers = JSON.parse(localStorage.getItem("users")!) || [];
        let user = {'name': userName, catchedPokemons: []};

        //only adds new user if user does not already exist.
        const userExists = allUsers.find( (user: any) => user.name === userName);
        if(userExists){
            user = userExists;
        }
        else{
            allUsers.push(user)
        }
        //sets localStorage
        localStorage.setItem("users", JSON.stringify(allUsers))
        localStorage.setItem("currentUser", JSON.stringify(user));  
        return this.loggedIn();
    }

    private loggedIn(){
        //direct to trainer page.
        return this.router.navigate(['trainer'])
    }

}