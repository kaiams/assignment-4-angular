import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { FetchPokemons } from "src/app/api/fetch-pokemons.api";
import { ApiResponsePokemon, Pokemon } from "src/app/shared/models/pokemon.model";
import {finalize, map} from 'rxjs/operators';
import { HttpErrorResponse } from "@angular/common/http";

@Component({
    selector: 'pokemon-deck-page',
    templateUrl: 'pokemon-deck.page.html',
    styleUrls: ['pokemon-deck.page.css']
})

export class PokemonDeckPage {
    readonly baseImageUrl = "../../../../../assets/pokemon-sprites/sprites/pokemon/other/official-artwork/"
    readonly user = JSON.parse(localStorage.getItem("currentUser")!);
    catchedPokemons: Pokemon[] = this.user.catchedPokemons;
    pokemons: Pokemon[] = [];
    next = "";
    moreAvailable= false;
   

    ngOnInit(){
        //If user is not logged in, redirect to login page
        if(!this.user){
            this.router.navigate(['']);
        }
    }
    constructor(private readonly router: Router, private readonly fetchPokemons: FetchPokemons) {
        //fetch on page load
        this.retrievePokemons("https://pokeapi.co/api/v2/pokemon");
    }

    public retrievePokemons(url: string){
        //gets the pokemons urls and fetches all data about that pokemon
        this.fetchPokemons.getPokemons$(url)
        .pipe(
            map((response: ApiResponsePokemon) => {
                if(!response) {
                    throw Error(`Pokemons was not found.`);
                }
                console.log(response.next)
                this.next = (response.next === null)? "" : response.next;
                if(this.next === ""){
                    this.moreAvailable = true;
                }
                return response.results;
          }),
          finalize(() => {
              console.log("done")
          }))
          .subscribe((pokemons: Pokemon[]) => {
                pokemons.map((pokemon) => {
                this.getPokemon(pokemon.url);
              },

            )}, 
            (error: HttpErrorResponse) => {
              console.log(error);
          });
    }

    public getPokemon(url: string){
        //fetches information about one pokemon
        this.fetchPokemons.getPokemonsIndividually$(url)
        .pipe(
            map((response: Pokemon) => {
                if(!response) {
                    throw Error(`Pokemons was not found.`);
            }
            return response;
          }),
          finalize(() => {
              console.log("done")
          }))
          .subscribe((pokemon: Pokemon) => {
                pokemon.imgUrl = this.baseImageUrl + pokemon.id +".png";
                pokemon.name = this.firstLetterToUpper(pokemon.name);
                pokemon.abilities.map(abilityInfo => abilityInfo.ability.name = this.firstLetterToUpper(abilityInfo.ability.name))
                pokemon.catched = this.catchedPokemons.some(caugth => 
                    pokemon.name === caugth.name);
                this.pokemons.push(pokemon);
          }, 
            (error: HttpErrorResponse) => {
              console.log(error);
          });
    }

    //Sets pokemon to catched and appends to the current users array. Updates the state currentUser in localStorage.
    //AllUsers will be updateted when the user logs out (to prevent updating the state all the time)
    public catchPokemon(pokemon: Pokemon){
        pokemon.catched= true;
        this.catchedPokemons.push(pokemon);
        localStorage.setItem("currentUser", JSON.stringify({name: this.user.name, catchedPokemons: this.catchedPokemons}));
    }

    //fetches 20 more pokemons with the url retrieved from last call.
    public loadMorePokemons(){
        this.retrievePokemons(this.next)
    }

    //returns word with first letter uppercase
    public firstLetterToUpper(word: string){
        if(word.length > 1){
            return word.slice(0, 1).toUpperCase() + word.slice(1);
        }
        return word.toUpperCase();
    }

}