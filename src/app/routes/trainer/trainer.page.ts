import { Component } from "@angular/core";
import { Router } from "@angular/router";


@Component({
    selector: 'trainer-page',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})

export class TrainerPage {
    readonly user = JSON.parse(localStorage.getItem("currentUser")!);
    readonly pokemons = this.user.catchedPokemons;

    ngOnInit() {
        //If user is not logged in, redirect to login page
        if(!this.user){
            this.router.navigate(['']);
        }
    }

    constructor(private readonly router: Router){

    }

}