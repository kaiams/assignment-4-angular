# Assignment4 - Pokemon catcher

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4.

Application to catch Pokemons.

A user has to first log in One can see the pokemom avatar, name and abilities. When the user logs out

## How to use:

1. Log in with name or username. You will be directed to the trainer page where all the Pokemons you've caught are displayed.
2. Go to catch Pokemons and click on the Pokemons to catch.
3. Log out, the Pokemons you've caught will be saved in local storage

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
